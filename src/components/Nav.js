import React from "react";
import { Link } from "gatsby";

const Nav = () => {
	return (
		<nav className="main-nav">
			<Link 
				to="/publications"
				className="main-nav-item"
				activeClassName="active"
			>
				Publications
			</Link>
			<Link
				to="/research"
				className="main-nav-item"
				activeClassName="active"
			>
				Research
			</Link>
			<Link
				to="/teaching"
				className="main-nav-item"
				activeClassName="active"
			>
				Teaching
			</Link>
			<Link
				to="/contact"
				className="main-nav-item"
				activeClassName="active"
			>
				Contact
			</Link>
		</nav>
	);
};

export default Nav;
