import React from "react";
import PropTypes from "prop-types";

const Footer = ({ title }) => {
	return (
		<footer id="footer" className="footer" role="contentinfo">
		</footer>
	);
};

Footer.propTypes = {
	title: PropTypes.string.isRequired,
};

export default Footer;
