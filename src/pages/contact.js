import React from "react";
import { Layout, SEO } from "../layout";

const SecondPage = () => (
	<Layout>
		<SEO title="Contact" />
		<section className="section-padding">
			<div className="container flow">
				<h2 className="major">Contact</h2>
				<ul>
					<li>
						<a href="https://scholar.google.com/citations?user=2upPg60AAAAJ&hl=en&oi=ao"> Google Scholar</a>
					</li>
					<li>
						<a href="https://github.com/maciek-tomczak"> Github</a>
					</li>
					<li>
						<a href="mailto:maciej.tomczak@bcu.ac.uk">Email</a>
					</li>
				</ul>
			</div>
		</section>
	</Layout>
);

export default SecondPage;
