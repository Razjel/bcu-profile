import React from "react";
import { Layout, SEO } from "../layout";

const IndexPage = () => (
	<Layout>
		<SEO title="Home" />
		<section className="section-padding">
			<div className="container flow">
				<h1>Maciej Tomczak</h1>
				<p>Hi, welcome to my Birmingham City University webspace.</p>
				<p>I am a PhD student in the <a href="http://somagroup.co.uk/">Sound and Music Analysis (SoMA) Group</a> within the <a href="https://www.bcu.ac.uk/computing/research/digital-media-technology">Digital Media Technology Laboratory (DMT Lab)</a>.</p>
				<p>My research interests are: Music information retrieval, rhythm analysis, deep learning, audio style transfer, audio synthesis, onset detection, beat and metre detection, drum transcription, digital audio effects, computational musicology, interactive music systems, music performance systems</p>
			</div>
		</section>
	</Layout>
);

export default IndexPage;
