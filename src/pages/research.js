import React from "react";
import { Layout, SEO } from "../layout";

const SecondPage = () => (
	<Layout>
		<SEO title="Research" />
		<section className="section-padding">
			<div className="container flow">
				<p>My work focuses on automated music transformations using deep learning, notably on neural drum synthesis, with an emphasis on introducing control over rhythmic manipulation of percussion recordings.</p>
				<ul class='fa-ul'>
					<li>
						<i>
							[<a href="https://maciek-tomczak.github.io/maciek.github.io/Drum-Synthesis-and-Rhythmic-Transformation/">demo</a>]
						</i> &nbsp;&nbsp;Drum Synthesis and Rhythmic Transformation with Adversarial Autoencoders [<a href="https://staff.aist.go.jp/m.goto/PAPER/ACMMM2020tomczak.pdf">paper</a>]
					</li>
					<li>
						<i>
							[<a href="https://maciek-tomczak.github.io/maciek.github.io/Drum-Translation-for-Timbral-and-Rhythmic-Transformation/">demo</a>]
						</i> &nbsp;&nbsp;Drum Translation for Timbral and Rhythmic Transformation [<a href="https://dafx2019.bcu.ac.uk/papers/DAFx2019_paper_25.pdf">paper</a>, {" "}
						<a href="https://maciek-tomczak.github.io/maciek.github.io/01ed740c6adf9500c3f986d78f3f00cd/dafx19-poster.pdf">poster</a>]
					</li>
					<li>
						<i>
							[<a href="https://maciek-tomczak.github.io/maciek.github.io/Audio-Style-Transfer-with-Rhythmic-Constraints/">demo</a>, {" "}
							<a href="https://github.com/maciek-tomczak/audio-style-transfer-with-rhythmic-constraints">code</a>]
						</i> &nbsp;&nbsp;Audio Style Transfer with Rhythmic Constraints [<a href="http://dafx.de/paper-archive/2018/papers/DAFx2018_paper_48.pdf">paper</a>]
					</li>
				</ul>
			</div>
		</section>
	</Layout>
);

export default SecondPage;
