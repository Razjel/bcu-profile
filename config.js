module.exports = {
	url: `https://dmtlab.bcu.ac.uk/maciejtomczak`,
	title: `Maciej's BCU Profile`,
	description: "",
	social: {
		image: `https://maciek-tomczak.github.io/`
	},
	author: {
		name: "Maciej Tomczak",
	},
	manifest: {
		name: `BCU Profile`,
		short_name: `Profile`,
		start_url: `/`,
		background_color: `#663399`,
		theme_color: `#663399`,
		display: `standalone`,
		icon: `src/images/logo.png`,
	},
	googleAnalyticsId: "UA-XXXXXXXX-X",
};
